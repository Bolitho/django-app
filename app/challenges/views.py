from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound;

# Create your views here.
data = {
  "January": "The Lord will guide you always; he will satisfy your needs in a sun-scorched land and will strengthen your frame. You will be like a well-watered garden, like a spring whose waters never fail. - Isaiah 58:11",
  "February": "Have I not commanded you? Be strong and courageous. Do not be afraid; do not be discouraged, for the Lord your God will be with you wherever you go. - Joshua 1:9",
  "March": "But those who hope in the Lord will renew their strength. They will soar on wings like eagles; they will run and not grow weary, they will walk and not be faint. - Isaiah 40:31",
  "April": "Therefore, if anyone is in Christ, the new creation has come: The old has gone, the new is here! - 2 Corinthians 5:17",
  "May": "The Lord is my strength and my shield; my heart trusts in him, and he helps me. My heart leaps for joy, and with my song I praise him. - Psalm 28:7",
  "June": "So do not fear, for I am with you; do not be dismayed, for I am your God. I will strengthen you and help you; I will uphold you with my righteous right hand. - Isaiah 41:10",
  "July": "I can do all this through him who gives me strength. - Philippians 4:13",
  "August": "And we know that in all things God works for the good of those who love him, who have been called according to his purpose. - Romans 8:28",
  "September": "Trust in the Lord with all your heart and lean not on your own understanding; in all your ways submit to him, and he will make your paths straight. - Proverbs 3:5-6",
  "October": "The name of the Lord is a fortified tower; the righteous run to it and are safe. - Proverbs 18:10",
  "November": "Give thanks to the Lord, for he is good; his love endures forever. - 1 Chronicles 16:34",
  "December": "For to us a child is born, to us a son is given, and the government will be on his shoulders. And he will be called Wonderful Counselor, Mighty God, Everlasting Father, Prince of Peace. - Isaiah 9:6"
}

def index(request, month):
    return HttpResponse("<h1>{}</h1>".format(data.get(month)))
def index_number(resquest, month):
    if month >12:
        return HttpResponseNotFound("<h1>Page not Found</h1>")
    months_lists= list(data.keys());
    return HttpResponseRedirect("/challenges/"+months_lists[month -1]);


